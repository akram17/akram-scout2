FROM rossja/ncc-scoutsuite
RUN ["/bin/bash", "-c", "/root/bin/container-aws-cmd.sh"]
# Remove scripts
RUN ["rm", "-rf", "/root/bin"]

# Command
CMD ["/bin/bash"]
